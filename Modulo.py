import re
def Comprar_placas():
	placa = input("Ingrese una placa: ")


	if re.match('(^T)([0-9]{5}$)', placa): 
	    print("Placa de Taxi")
	elif re.match('(^M)(B)([0-9]{3}$)', placa):
	    print("Placa de metro bus")
	elif re.match('(^B)([0-9]{5}$)', placa):
	    print("Placa de Bus")
	elif re.match('(^M)([0-9]{5}$)', placa):
	    print("Placa de Moto")
	elif re.match('(^M)([0-9]{4}$)', placa):
	    print("Placa de Moto")
	elif re.match('(^D)([0-9]{5}$)', placa):
	    print("Placa de automovolis de demostracion o prueba")
	elif re.match('(^P)(R)([0-9]{4}$)', placa):
	    print("Placa de periodista")
	elif re.match('(^C)(D)([0-9]{5}$)', placa):
	    print ("Placa de Cuerpo Diplomatico")
	elif re.match('(^E)([0-9]{5}$)', placa):
	    print("Placa de Juez o Fiscal")
	elif re.match('(^C)(C)([0-9]{4}$)', placa):
	    print("Placa de Cuerpo Consular ")
	elif re.match('(^[A-Z-0-9])([0-9]{6}$)', placa):
	    print("Placa de autos particulares")
	else:
	    print("Placa de auto no valida")